var container, scene, renderer, camera, light, cube,
controls, position = 0, rotation = false, name, zoom = 1.0, sum = -0.01, _zoom = false;
var WIDTH, HEIGHT, VIEW_ANGLE, ASPECT, NEAR, FAR;
	var object = 'js/sinon-sword-art-online.json';
$(document).ready(function() {
	
	//var object = 'js/sinon-sword-art-online.json';
	name = 'lamborghini';
	WIDTH = $('.viewport').width(),
	HEIGHT = $('.viewport').height();

	// Rotate an object around an arbitrary axis in object space
	var rotObjectMatrix;

	VIEW_ANGLE = 45,
	ASPECT = WIDTH / HEIGHT,
	NEAR = 1,
	FAR = 10000;
	var a = false;
	container = document.querySelector('.viewport');

	var loader = new THREE.ObjectLoader();
	// Cargamos el modelo.

	loader.load(object,postLoadObj);
	
	$('.toogle-rotation').click(function() {
		rotation = !rotation;
	});
	
	$('.toogle-zoom').click(function() {
		_zoom = !_zoom;
	});
})

/**
	Operaciones con el modelado tras su carga.
 */
function postLoadObj(obj) {
	obj.name = name;
	// Cargamos materiales
		scene = new THREE.Scene();
		renderer = new THREE.WebGLRenderer({antialias: true, autoclear: true, alpha: true});
		renderer.setSize(WIDTH, HEIGHT);
		renderer.shadowMapEnabled = true;
		renderer.shadowMapSoft = true;
		renderer.shadowMapType = THREE.PCFShadowMap;
		renderer.shadowMapAutoUpdate = true;

		container.appendChild(renderer.domElement);


		camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
		camera.position.set(10, 10, 10);
   		 camera.lookAt(scene.position);

		light = new THREE.DirectionalLight(0xffffff);

		light.position.set(0, 100, 60);
		light.castShadow = true;
		light.shadowCameraLeft = -60;
		light.shadowCameraTop = -60;
		light.shadowCameraRight = 60;
		light.shadowCameraBottom = 60;
		light.shadowCameraNear = 1;
		light.shadowCameraFar = 1000;
		light.shadowBias = -.0001
		light.shadowMapWidth = light.shadowMapHeight = 1024;
		light.shadowDarkness = 0;

		scene.add(light);
		//var mesh = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));
		scene.add(obj);
		/*cube = new THREE.Mesh(
		  new THREE.CubeGeometry(10, 10, 10),
		  new THREE.MeshLambertMaterial({
		    color: 0xff0000
		  })
		);

		scene.add(cube);*/
		
		renderFrames();
}

function renderFrames() {
  render();
  checkRotation();
  checkZoom();
  setTimeout(renderFrames, 50);
}

function renderControls() {
	controls = new THREE.OrbitControls( camera );
	controls.addEventListener( 'change', render );
	render();
}

function render() {
  renderer.render(scene, camera);
}

function checkRotation(){
    var x = camera.position.x,
        y = camera.position.y,
        z = camera.position.z;
        position = 0.2;
    if (!rotation) {
        camera.position.x = x * Math.cos(position) - z * Math.sin(position);
        camera.position.z = z * Math.cos(position) + x * Math.sin(position);
        camera.lookAt(scene.position);
    }
}

function checkZoom() {
	if (!_zoom) {
		zoom += sum;
		camera.fov *= zoom;
		if (zoom <= 0.8 || zoom >= 1.0 ) {
			sum = -sum;
			camera.fov = VIEW_ANGLE;
		}
		camera.updateProjectionMatrix();
	}
}